import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class DeSerrObj {

    SerClassToWork serObj = new SerClassToWork();
    byte[] buff;

    private ByteArrayOutputStream deSerObj(ByteArrayOutputStream buff) throws IOException, InvocationTargetException, IllegalAccessException, NoSuchMethodException {

        System.out.println("DeSerrObj class execution started...");
        this.buff = buff.toByteArray();
        // creating input stream
        ByteArrayInputStream bais = new ByteArrayInputStream(this.buff);
        ObjectInputStream ois = new ObjectInputStream(bais);

        // deserializing object
        System.out.println("Deserializing in DeSerrObj class...");
        try {
            serObj = (SerClassToWork) ois.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        // changing object values
        Class serClass = SerClassToWork.class;

        Class[] paramTypes = new Class[]{};
        Method method = serClass.getDeclaredMethod("setAge", paramTypes);
        method.setAccessible(true);
        method.invoke(serObj);

      //  System.out.println("Variable after serializing and before changes "+serObj.getAge());
        // serObj.setAge((int) (Math.random()*100));
     //   System.out.println("Variable after serializing and after changes "+serObj.getAge());

        // initializing output stream
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);

        // serializing object
        System.out.println("Serializing in DeSerrObj...");
        oos.writeObject(serObj);
        // closing stream
        oos.flush();
        baos.flush();
        oos.close();
        baos.close();
        System.out.println("Completed class DeSerrObj execution...");
        return baos;

    }
}
