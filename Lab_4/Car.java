/**
 * Created by ai on 4/14/2016.
 */
// class Car with general description

public abstract class Car {
    public double v0 = 0;
    public double speedMax, a, k;
    public double time;
    public double v1;
    public double speedMaxSec;

    // конструктор объекта с заданием входящих параметров
    Car(double speedMax, double a, double k) {
        this.speedMax = speedMax;
        this.a = a;
        this.k = k;
        this.speedMaxSec = speedMax * 1000 / 3600;
    }

    // реализуем гонку
    public double race() {
        time = 0;
        for (int i = 1; i <= 20; i++) {
            moveStraight();
            moveTurn();
        }
        return time;
    }

    // движение прямо
    public void moveStraight() {

        v1 = v0 + Math.sqrt((v0 * v0) + (2 * a * 2000));

        if (v1 > speedMaxSec) {
            v1 = speedMaxSec;
        }

        // check if max speed same as start speed and count time
        if ((v1 - v0) > 0) {
            time = time + ((v1 - v0) / a);
        } else {
            time = time + (2000 / v1);
        }
    }

    // поворот
    public void moveTurn() {
        v0 = v1 * k;
    }
}
