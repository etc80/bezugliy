
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by ai on 4/11/2016.
 */
public class Lab3Test5 {
    public static void main(String[] args) throws IOException {
        // описание, что делать
        System.out.println("Вводите числа для подсчета их суммы. Для вывода результатов введите СУММА.");
        // инициализируем ввод
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        // объявляем переменные для ввода и подсчета
        String inputString = "";
        float summ = 0;
        // ввод с консоли пока не будет введено слово Сумма
        while (!inputString.equalsIgnoreCase("СУММА")) {
            inputString = br.readLine();
            // если введена сумма прерываем цикл
            if (inputString.equalsIgnoreCase("СУММА")) {
                break;
            }
            // если не сумма, переводим строку в число и сумируем
            else {
                float lastNum = 0;
                try {
                    lastNum = Float.parseFloat(inputString);
                } catch (NumberFormatException e) {
                    System.err.println("Incorrect number format!");
                }
                summ = summ + lastNum;
            }
        }
        // вывод результатов
        System.out.print("Сумма всех введенных чисел равна ");
        if(summ%1 != 0) {
            // если число получилось дробное
            System.out.println(summ);
        }
        else
        {
            // если число получилось целое
            System.out.print((int) summ);
        }
    }
}
