import java.io.IOException;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by ai on 4/26/2016.
 */
public class Lab6Test23 {

    static Queue<Integer> queue = new PriorityQueue();
    static int randomNumber;
    static Object sync = new Object();

    public static void main(String[] args) throws IOException, InterruptedException {

        Thread threadEven = new Thread(new CheckEven());
        Thread threadOdd = new Thread (new CheckOdd());
        Thread threadSum = new Thread (new Summa());
        threadEven.start();
        threadOdd.start();
        threadSum.start();


        for (int i = 0; i< 10; i++) {
            synchronized (sync) {
                randomNumber = (int) (Math.random() * 101);
                queue.add(randomNumber);
                System.out.println(randomNumber);
                sync.notifyAll();
            }
        }
    }

    public static class CheckEven implements Runnable{
        private Object sync = randomNumber;

        @Override
        public void  run() {
                synchronized (sync) {
                for (int i = 0; i < 10; i++) {
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (randomNumber % 2 == 0)
                        System.out.println("Number is even.");
                }
            }
        }
    }

    static class CheckOdd  implements Runnable {
        private Object sync = randomNumber;

        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                synchronized (sync) {
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (randomNumber % 2 != 0)
                        System.out.println("Number is odd.");
                }
            }
        }
    }

    static class Summa implements Runnable {
        private Object sync = randomNumber;
        private int sum = 0;

        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                synchronized (sync) {
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    sum = sum+randomNumber;
                    System.out.println("Sum of all elements in Queue is " + sum);
                }
            }
        }
    }
}
