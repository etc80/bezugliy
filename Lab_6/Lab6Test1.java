import java.io.*;

/**
 * Created by ai on 4/20/2016.
 * Class for testing IO module and threads
 */
public class Lab6Test1 {
    public static void main(String[] args) throws IOException {

        String fileName;

        InputStream inputStream = System.in;
        Reader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        do {
            System.out.println("*** Input file name to copy, or command 'end' to close program ***");
            fileName = bufferedReader.readLine();
            if(!fileName.equalsIgnoreCase("end")) {
                MyThread myThread = new MyThread();
                Thread fileCopyThread = new Thread(myThread);
                myThread.fileName = fileName;
                fileCopyThread.start();
            }
        } while (!fileName.equalsIgnoreCase("end"));

        System.out.println();

    }
}
