import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 Написать сериализуемый класс с методом меняющим состояние объекта.
 В мэйне создать объект этого класса, сериализовать, десериализовать,
 с помощью рефлексии вызвать метод меняющий состояние.
 */

public class Lab7Test1 {

    //creating object
    static SerClassToWork objectToSer = new SerClassToWork();

    public static void main(String[] args) throws IOException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {

        // serializing and deserializing object
        deSerializingObject(serializingObject());


        // change object state with reflection
        System.out.println("Start object method reflection...");
        Class serClass = SerClassToWork.class.getSuperclass();
        Class[] paramTypes = new Class[]{};
        Method method = serClass.getDeclaredMethod("changeAge", paramTypes);
        method.setAccessible(true);
        method.invoke(objectToSer);
        System.out.println("Completed reflection object method");
       // System.out.println("Object field after changing " + objectToSer.getAge());

    }

    public static ByteArrayOutputStream serializingObject() throws IOException {
        System.out.println("Serializing object started...");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(objectToSer);
        oos.flush();
        oos.close();
        System.out.println("Serializing object completed...");
        return baos;
    }

    public static int deSerializingObject(ByteArrayOutputStream buff) throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        // deserializing object
        System.out.println("Deserializing object started...");
        ByteArrayInputStream bais = new ByteArrayInputStream(buff.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bais);
        try {
            objectToSer = (SerClassToWork) ois.readObject();
            Class serClass = SerClassToWork.class.getSuperclass();
            Class[] paramTypes = new Class[]{};
            Method method = serClass.getDeclaredMethod("getAge", paramTypes);
            method.setAccessible(true);
            System.out.println("Value in object after deserializing is " + method.invoke(objectToSer));
            return (int) method.invoke(objectToSer);
   //         System.out.println("Value in object after deserializing is " + objectToSer.getAge());
  //          return objectToSer.getAge();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return -1;
        } finally {
            System.out.println("Deserializing object completed...");
        }
    }


}
