import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.*;

/**
 * Created by ai on 4/29/2016.
 */
public class SerClassTest {

    SerClassToWork obj = new SerClassToWork();

    @Test
    public void testSetAge() throws Exception {



        Class serClass = SerClass.class;
        Class[] paramTypes = new Class[]{int.class};
        Method method = serClass.getDeclaredMethod("setAge", paramTypes);
        method.setAccessible(true);
        Object[] args = new Object[] { new Integer(101) };
        method.invoke(obj,args);

     // obj.setAge(101);

        paramTypes = new Class[]{};
        method = serClass.getDeclaredMethod("getAge",paramTypes);
        method.setAccessible(true);
        int i = (int) method.invoke(obj);
   //     int i = obj.getAge();
        assertTrue (i == 101);
    }

    @Test
    public void testGetAge() throws Exception {

        Class serClass = SerClass.class;
        Class[] paramTypes = new Class[]{};
        Method method = serClass.getDeclaredMethod("getAge", paramTypes);
        method.setAccessible(true);
        int i = (int) method.invoke(obj);

     //   int i = obj.getAge();
        assertTrue (i >= 0 && i <= 100);

    }

    @Test
    public void changeAge() throws Exception {

        Class serClass = SerClass.class;
        Class[] paramTypes = new Class[]{};
        Method method = serClass.getDeclaredMethod("changeAge", paramTypes);
        method.setAccessible(true);
        method.invoke(obj);

        method = serClass.getDeclaredMethod("getAge",paramTypes);
        method.setAccessible(true);
        int i = (int) method.invoke(obj);
     //   obj.changeAge();
     //    int i = obj.getAge();
        assertTrue (i == 50);
    }


}