/**
 * Created by ai on 4/14/2016.
 */
    /*
    *3ий - если автомобиль в момент поворота достиг своей максимальной скорости,
    * то его максимальная скорость увеличивается на 10% до конца гонки
    */
public class CarThree extends Car
{
    CarThree(double speedMax, double a, double k) {
        super(speedMax, a, k);
    }
    boolean rushSpeed = false;
    @Override
    public void moveStraight() {

        v1 = v0 + Math.sqrt((v0 * v0) + (2 * a * 2000));

        if (v1 > speedMaxSec) {
            v1 = speedMaxSec;
            if (!rushSpeed)
            {
                speedMaxSec = speedMaxSec + speedMaxSec*0.1;
                rushSpeed = true;
            }
        }

        // check if max speed same as start speed and count time
        if ((v1 - v0) > 0) {
            time = time + ((v1 - v0) / a);
        } else {
            time = time + (2000 / v1);
        }
    }
}
