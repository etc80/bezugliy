import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by ai on 4/21/2016.
 */
public class MyThread implements Runnable {

    String fileName;
    String defaultPath = "files\\";
    List<String> allFolders = new ArrayList<>();
    List<String> filesToCopy = new ArrayList<>();

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run()  {

        createFoldersTree();
        findFiles();
        for(int i =0; i<filesToCopy.size(); i++)
        {

            File source = new File(filesToCopy.get(i));

            int folderNumber = 1;
            while (true) {
                File checkDir = new File(source.getParent()+folderNumber);
                if (checkDir.exists()) {
                folderNumber++; } else { break; }
            }

            String destPath = source.getParent() + folderNumber;
            File destDir = new File(destPath);
            destDir.mkdirs();

            String destFile = destPath+"\\"+source.getName();
            File dest = new File(destFile);
            try {
                copyFiles(source, dest);
            } catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    public static void copyFiles (File source, File dest) throws IOException {
        Files.copy(source.toPath(), dest.toPath());
    }

    public boolean hasFolders(String path){
        boolean hasFolder = false;
        String[] list = new File(path).list();
        for (String aList : list) {
            File fullName = new File (path+aList.toString());
            if (fullName.isDirectory())
            {
                hasFolder=true;
                break;
            }
        }
        return hasFolder;
    }

    public void createFoldersTree(){

        allFolders.add(defaultPath);

        for (int i = 0; i< allFolders.size();i++){

            String tempPath = allFolders.get(i).toString();

            String[] subDir = new File(tempPath).list();
            if (subDir!=null && hasFolders(tempPath))
            {
                for (String nList : subDir) {
                    File tempName = new File(tempPath+nList.toString());
                    if (tempName.isDirectory()) {
                        allFolders.add(tempName.getAbsolutePath()+"\\");
                    }
                }
            }
        }
    }

    public void findFiles(){

        for (String path : allFolders) {
            if (hasFiles(path)) {
                String[] files = new File(path).list();
                if (files != null && hasFiles(path)) {
                    for (String aFiles : files) {
                        File fullName = new File(path + aFiles.toString());
                        if (fullName.isFile() && fullName.getName().toLowerCase().contains(fileName.toLowerCase())) {
                            filesToCopy.add(fullName.getAbsolutePath());
                        }
                    }
                }
            }
        }
    }

    private boolean hasFiles(String path) {
       boolean hasFiles = false;
        String[] list = new File(path).list();
        for (String aList : list) {
            File fullName = new File (path+aList.toString());
            if (fullName.isFile())
            {
                hasFiles=true;
                break;
            }
        }
        return hasFiles;
    }


}
