
/**
 * Created by pitbot on 4/7/2016.
 */
public class Perevod2 {
    public static void main(String[] args)
    {
        // int to float
        int i = 2015;
        float f = (float) (i);
        System.out.println(f);
        // float to int
        float number = 444.33f;
        long aValue = (long) number;
        System.out.println(aValue);
    }
}
