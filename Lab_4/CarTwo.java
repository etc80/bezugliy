/**
 * Created by ai on 4/14/2016.
 */

 /*
    * 2ой - если скорость автомобиля после поворота меньше чем половина от максимальной то его ускорение на этом отрезке увеличивается в 2 раза
    */
public class CarTwo extends Car
{
    CarTwo(double speedMax, double a, double k) {
        super(speedMax, a, k);
    }
    int ak = 1;
    @Override
    public void moveStraight() {

        if (v0 < (speedMaxSec/2)) {
            ak = 2;
        } else {
            ak = 1;
        }
        v1 = v0 + Math.sqrt((v0 * v0) + (2 * a * ak * 2000));
        if (v1 > speedMaxSec) {
            v1 = speedMaxSec;
        }

        // check if max speed same as start speed and count time
        if ((v1 - v0) > 0) {
            time = time + ((v1 - v0) / a);
        } else {
            time = time + (2000 / v1);
        }
    }
}
