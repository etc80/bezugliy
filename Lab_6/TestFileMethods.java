import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by ai on 4/21/2016.
 */
public class TestFileMethods {
    public static void main(String[] args) throws IOException {

        File file = new File("files//testFile.txt");
        if (!file.exists()) System.out.println("file not found!");
        System.out.println("getName - " + file.getName());
        System.out.println("getParent - " + file.getParent());
        System.out.println("getParentFile - " + file.getParentFile());
        System.out.println("getPath - " + file.getPath());
        System.out.println("isAbsolute " + file.isAbsolute());
        System.out.println("getAbsolutePath " + file.getAbsolutePath());
        System.out.println("getAbsoluteFile " + file.getAbsoluteFile());
        System.out.println("getCanonicalPath " + file.getCanonicalPath());
        System.out.println("getCanonicalFile " + file.getCanonicalFile());
        System.out.println("toURI - " + file.toURI());
        System.out.println("canRead " + file.canRead());
        System.out.println("canWrite " + file.canWrite());
        System.out.println("exists " + file.exists());
        System.out.println("isDirectory " + file.isDirectory());
        System.out.println("isFile " + file.isFile());
        System.out.println("isHidden " + file.isHidden());
        System.out.println("lastModified " + file.lastModified());
        System.out.println("lenght " + file.length());
        System.out.println("createNewFile " + file.createNewFile());
        System.out.println("list " + file.list());
        System.out.println("list filter " + Arrays.toString(file.list(null)));
        System.out.println("mkdir " + file.mkdir());
        System.out.println("mkdirs " + file.mkdirs());
        System.out.println("canExecute " + file.canExecute());
        System.out.println("listRoots " + Arrays.toString(file.listRoots()));
        System.out.println("getTotalSpace " + file.getTotalSpace());
        System.out.println("getFreeSpace " + file.getFreeSpace());
        System.out.println("getUsableSpace " + file.getUsableSpace());
        System.out.println("hashCode " + file.hashCode());
        System.out.println("toString " + file.toString());
        System.out.println("toPath " + file.toPath());
    }
}
