
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by ai on 4/11/2016.
 */
/*
Вывести все простые числа от 1 до N. N задается пользователем через консоль
 */
public class Lab3Test3 {
    public static void main(String[] args) throws IOException {
        // read last number
        System.out.println("Input last number");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s1 = br.readLine();
        // transfer to integer
        int lastNum = 0;
        try {
            lastNum = Integer.parseInt(s1);
        }
        catch (NumberFormatException e)
        {
            System.err.println("Incorrect number format!");
        }
        // check if last number is bigger than first
        if (lastNum > 1)
        {
            // print first primitive number - 2
            System.out.println("2");
            // find and output all other primitive numbers
            for (int i = 3; i < lastNum; i=i+2)
            {
                    boolean isPrimitive = true;
                    int count = 2;
                    while (isPrimitive && count <= (i/2))
                    {
                        isPrimitive = (i%count != 0);
                        count++;
                    }
                    if (isPrimitive)
                    {
                        System.out.println(i);
                    }
            }
        }
        else
        {
            // if last number is less than first, notify this
            System.out.println("Last number should be bigger than 1!");
        }

    }
}
