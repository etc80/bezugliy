import java.io.IOException;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by ai on 4/22/2016.
 *
 * Написать программу в которой было бы 4 потока и очередь целых чисел.
 * Задача потоков:
 * 1 - основной - заполняет очередь числами,
 * 2 - если число четное - вывести его на экран,
 * 3 - если число нечетное вывести его на экран,
 * 4 - суммировать все числа и после того как 2 или 3 потоки вывели на экран инфомрацию вывести сумму всех чисел очереди
 * от 1го до того которое рассматривается сейчас

 */
public class Lab6Test2 {
    public static void main(String[] args) throws IOException, InterruptedException {

        // создаем очередь
        final Queue<Integer> queue = new PriorityQueue();

        // бесконечный цикл по добавлению случайного числа в очередь и проверке его на четность
        while (System.in.available() == 0) {
            // генерируем случайное число
            final int randomNumber = (int) (Math.random()*101);
            System.out.println("random number to add "+randomNumber);
            // добавляем число в очередь
            queue.add(randomNumber);
            System.out.println("size " + queue.size());

            // создаем поток проверки числа на четность
            Thread checkEven = new Thread (new Runnable() {

                @Override
                public void run() {
                    if (randomNumber %2 == 0)
                     System.out.println("Number is even.");
                }
            });
            // запускаем поток
            checkEven.start();

            // создаем и запускаем поток проверки числа на нечетность
            Thread checkOdd = new Thread (new Runnable() {

                @Override
                public void run() {
                    if (randomNumber %2 != 0)
                    System.out.println("Number is odd.");
                }
            });
            checkOdd.start();

            // создаем ожидание главным потоком окончания работы потоков обработки
            checkEven.join();
            checkOdd.join();

            // создаем и запускаем поток сумирующий результаты
            Thread sum = new Thread(new Runnable() {
                @Override
                public void run() {
                    int sum = 0;
                    Integer[] allNumbers = new Integer[queue.size()];
                    allNumbers = queue.toArray(allNumbers);
                    for (Integer allNumber : allNumbers) {
                        sum = sum + allNumber;
                    }
                    System.out.println("Sum of all elements in Queue is "+ sum);
                }
            });
            sum.start();
            // ожидаем главным потоком завершения выполнения потока сумирования
            sum.join();

            // задержка, чтоб успевать следить за процессом
            System.out.println("Press ENTER to exit!!!");
            Thread.sleep(1000);

        }
    }
}

