/**
 * Created by ai on 4/11/2016.
 */
public class Lab3Test1 {
    public static void main(String[] args) {
        // объявляем массив
        int mass[] = {53, 99, 21, 16};
        // создаем временную переменную для сравнения
        int x =mass[0];
        // цикл сравнения
        for (int i = 0; i < mass.length; i++) {
            if (mass[i] < x) {
                x = mass[i];
            }
        }
        // вывод результата на экран
        System.out.println("Наименьшее число из массива "+ x);
    }
}
