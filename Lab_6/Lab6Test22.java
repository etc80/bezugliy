import java.io.IOException;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by ai on 4/25/2016.
 */

public class Lab6Test22 {

        static Queue<Integer> queue = new PriorityQueue();
        static int randomNumber;

    public static void main(String[] args) throws IOException, InterruptedException {

        Thread threadEven = new Thread(new CheckEven());
        Thread threadOdd = new Thread (new CheckOdd());
        Thread threadSum = new Thread (new Summa());
        threadEven.start();
        threadOdd.start();
        threadSum.start();
        Thread.sleep(500);


        for (int i = 0; i< 10; i++) {
            randomNumber = (int) (Math.random() * 101);
            queue.add(randomNumber);
            System.out.println(randomNumber);
            Thread.sleep(1000);

        }
    }

     public static class CheckEven implements Runnable{

        @Override
        public void  run() {
            for (int i = 0; i < 10; i++) {
                try {
                    Thread.sleep(980);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (randomNumber % 2 == 0)
                    System.out.println("Number is even.");
            }
        }
    }

    static class CheckOdd  implements Runnable {

        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (randomNumber % 2 != 0)
                    System.out.println("Number is odd.");

            }
        }
    }

    static class Summa implements Runnable {

        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {

                try {
                    Thread.sleep(1020);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                int sum = 0;
                Integer[] allNumbers = new Integer[queue.size()];
                allNumbers = queue.toArray(allNumbers);
                for (Integer allNumber : allNumbers) {
                    sum = sum + allNumber;
                }
                System.out.println("Sum of all elements in Queue is " + sum);
            }
        }
    }
}


