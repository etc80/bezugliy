/**
 * Created by ai on 4/14/2016.
 */
    /*
     У каждого автомобиля есть своя особенность
    1ый - если скорость автомобиля на момент поворота больше половины от максимальной -
    то он получает +0,5% от разницы между половиной от максимальной и нынешней к маневренности
    на этот поворот следующим образом (половина максимальной = 75. Нынешняя равна 90. Разница = 15.
    Маневренность до срабатывания = 0,5, стала = 0,575)
    */

public class CarOne extends Car
{
    CarOne(double speedMax, double a, double k) {
        super(speedMax, a, k);
    }
    @Override
    public void moveStraight() {

        v1 = v0 + Math.sqrt((v0 * v0) + (2 * a * 2000));

        if (v1 > speedMaxSec) {
            v1 = speedMaxSec;
        }
        // условие для первой машины
        if (v1 > (speedMaxSec/2))
        {
            k = k + (v1*3600/1000 - speedMax/2)*0.5/100;
        }
        // check if max speed same as start speed and count time
        if ((v1 - v0) > 0) {
            time = time + ((v1 - v0) / a);
        } else {
            time = time + (2000 / v1);
        }
    }

}

