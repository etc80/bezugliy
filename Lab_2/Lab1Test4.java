
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by pitbot on 4/7/2016.
 */
public class Lab1Test4 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        // read first number
        System.out.println("Input number");
        String s1 = br.readLine();
        // read operation
        System.out.println("Input operation: + , - , * , /");
        String s2 = br.readLine();
        // read second number
        System.out.println("Input number");
        String s3 = br.readLine();
        // output result
        System.out.print("Result is: ");
        float f1 = 0;
        // check if first number is not empty
        if (s1.length() > 0)
        {
            f1 = new Float(s1);
        }
        float f2 = 0;
        // check if second number is not empty
        if (s3.length() > 0)
        {
            f2 = new Float(s3);
        }
        // choose operator to use between numbers
        switch (s2) {
            case "+": {
                if ((f1 + f2) % 1 == 0) {
                    System.out.println((int)(f1+f2));
                }
                else {
                    System.out.println(f1 + f2);
                }
                break;
            }
            case "-": {
                if ((f1 - f2) % 1 == 0) {
                    System.out.println((int) (f1 - f2));
                }
                else {
                    System.out.println(f1 - f2);
                }
                break;
            }
            case "*": {
                if ((f1 * f2) % 1 == 0) {
                    System.out.println((int) (f1 * f2));
                }
                else
                {
                    System.out.println(f1 * f2);
                }
                break;
            }
            case "/": {
                if ((f1 / f2) % 1 == 0) {
                    System.out.println((int) (f1 / f2));
                }
                else {
                    System.out.println(f1 / f2);
                }
                break;
            }
            default: {
                System.out.print("Incorrect operation entered");
            }
        }
    }


}
