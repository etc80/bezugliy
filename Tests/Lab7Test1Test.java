import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created by ai on 4/29/2016.
 */
public class Lab7Test1Test {

    @Test
    public void testSerializingObject() throws Exception {

        ByteArrayOutputStream buff = Lab7Test1.serializingObject();
        assertTrue(buff.toByteArray().length > 0);
        System.out.println("*** End of testSerializingObject test ***");
    }

    @Test
    public void testDeSerializingObject() throws Exception {
        ByteArrayOutputStream buff = Lab7Test1.serializingObject();
        assertTrue (Lab7Test1.deSerializingObject(buff) >= 0);
        System.out.println("*** End of testDeSerializingObject test ***");
    }
}