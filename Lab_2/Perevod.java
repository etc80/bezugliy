
/**
 * Created by pitbot on 4/7/2016.
 */
public class Perevod {
    public static void main(String[] args)
    {
        // integer
        int i = 35;
        String str = Integer.toString(i);
        System.out.println(str);
        Integer i2 = Integer.valueOf(str);
        System.out.println(i2);
        // double
        double  d = 32.4e10;
        String std = Double.toString(d);
        System.out.println(std);
        Double d2 = Double.valueOf(std);
        System.out.println(d2);
        // long
        long  l = 3422222;
        String stl = Long.toString(l);
        System.out.println(stl);
        Long l2 = Long.valueOf(stl);
        System.out.println(l2);
        // float
        float  f = 3.46f;
        String stf = Float.toString(f);
        System.out.println(stf);
        Float f2 = Float.valueOf(stf);
        System.out.println(f2);
        // char
        char ch = 'S';
        String stc = "" + ch;
        System.out.println(stc);
        char ch1 = stc.charAt(0);
        System.out.println(ch1);
        // boolean
        Boolean boolean1;
        String stbo = "true";
        boolean1 = Boolean.valueOf(stbo);
        System.out.println( boolean1 );
    }
}
