import java.io.Serializable;

/**
 * Created by ai on 4/27/2016.
 */
public class SerClass implements Serializable{

    private int age;


    // Конструктор
    protected SerClass() {
        age = (int)(Math.random() * 100);
        System.out.println("field value after creating is "+age);
    }

    private int getAge() {
        return age;
    }

    private void setAge(int age) {
        this.age = age;
    }

    private void changeAge () {
        age = 50;
    }


}
