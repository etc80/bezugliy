

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by ai on 4/13/2016.
 */
public class Lab5Test1 {
    public static void main(String[] args) throws ferialException {

        MyList<Date> dates = new MyList(30);
        Date date = new Date();
        long currentDate = date.getTime();
        for (int i = 0; i< dates.size(); i++)
        {
            long randomDate = (long) (Math.random()*currentDate);
            dates.set(i, new Date(randomDate));
            System.out.println("date " + i + " is " + dates.get(i));
            try {
                checkDate(dates.get(i));
            } catch (ferialException e)
            {
                System.err.println("Date " + dates.get(i).toString() + " is ferial!");
            } catch (holidayException e)
            {
                System.err.println("Date " + dates.get(i).toString() + " is holiday!");
            }

        }
    }



    public static void checkDate(Object date) throws ferialException, holidayException {
        String simpleDate = new SimpleDateFormat("EEE").format(date);
        if(simpleDate.equals("Sat") || simpleDate.equals("Sun")) {
            throw new holidayException("Date " + date.toString() + " is holiday!");
        }
        else {
            throw new ferialException("Date " + date.toString() + " is ferial!");
        }
    }

    public static class ferialException extends Exception
    {
        public ferialException(String s)
                {
                    super();
                }

        public ferialException() {
            super();
        }
    }
    public static class holidayException extends RuntimeException
    {
        public holidayException(String s) {
            super();
        }
    }
}
