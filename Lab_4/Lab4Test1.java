

/**
 * Created by ai on 4/11/2016.
 */
public class Lab4Test1 {
    public static void main(String[] args)
    {
        for (int i = 1; i <= 5; i++) {
            System.out.println("************   Race # "+i+" start ************");
            Car firstCar = new CarOne(Math.random()*300+1, Math.random()*5+0.1, Math.random());
            Car secondCar = new CarTwo(Math.random()*300+1, Math.random()*5+0.1, Math.random());
            Car thirdCar = new CarThree(Math.random()*300+1, Math.random()*5+0.1, Math.random());
            sortRaceTime(firstCar.race(), secondCar.race(), thirdCar.race());
            System.out.println("----------------- Race # "+i+" ended -----------------");
        }
    }

    public static void sortRaceTime(double t1, double t2, double t3)
    {
        double[] sortMass = new double[3];
        sortMass[0] = t1;
        sortMass[1] = t2;
        sortMass[2] = t3;
        int [] autoNumber = {1, 2, 3};
        for (int i = 0; i < sortMass.length-1; i++)
        {
            for (int x = 0; x <= sortMass.length-2; x++)
            {
                if (sortMass[x] > sortMass[x+1])
                {
                    double temp = sortMass[x+1];
                    sortMass[x+1] = sortMass[x];
                    sortMass[x] = temp;
                    int temp2 = autoNumber[x+1];
                    autoNumber[x+1] = autoNumber[x];
                    autoNumber[x] = temp2;
                }
            }
        }
        System.out.print("Автомобиль "+autoNumber[0]+" пришел трассу за ");
        timeToPrint(sortMass[0]);
        System.out.print("Автомобиль "+autoNumber[1]+" пришел трассу за ");
        timeToPrint(sortMass[1]);
        System.out.print("Автомобиль "+autoNumber[2]+" пришел трассу за ");
        timeToPrint(sortMass[2]);
    }
    public static void timeToPrint (double timeToTransform)
    {
        int hours;
        int minutes;
        int seconds;
        Integer timeInt = (int)timeToTransform;
        if(timeInt >= 3600)
        {
            hours = timeInt/3600;
            System.out.print(hours + " ч. ");
        }
        if (timeInt >= 60)
        {
            minutes = (timeInt%3600)/60;
            System.out.print(minutes + " мин. ");
        }
        if(timeInt >0)
        {
            seconds = ((timeInt%3600)%60);
            System.out.println(seconds + " сек.");
        }
    }
}
